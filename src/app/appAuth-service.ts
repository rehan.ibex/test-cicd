import { Injectable, OnDestroy, Inject } from '@angular/core';
import { OidcSecurityService, OpenIdConfiguration, AuthWellKnownEndpoints, AuthorizationResult, AuthorizationState } from 'angular-auth-oidc-client';
import { Observable ,  Subscription, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable()
export class AuthService implements OnDestroy {

    isAuthorized = false;

    constructor(
        private oidcSecurityService: OidcSecurityService,
        private http: HttpClient,
        private router: Router
    ) {
    }

    private isAuthorizedSubscription: Subscription = new Subscription;

    ngOnDestroy(): void {
        if (this.isAuthorizedSubscription) {
            this.isAuthorizedSubscription.unsubscribe();
        }
    }

      // private stsURL = "https://localhost:44328";
  private stsURL = "https://localhost:44352";
  private uRL = "https://localhost:4200";

    public initAuth() {

        const config: OpenIdConfiguration = {
            stsServer: this.stsURL,
            redirect_url: this.uRL,// + '/forbidden',
            client_id: 'codeflowpkceclient',
            response_type: 'code', // 'id_token token' Implicit Flow
            scope: 'openid profile offline_access email ProtectedApi',
            post_logout_redirect_uri: this.uRL,
            //start_checksession: true,
            // silent_renew: true,
            // silent_renew_url: this.uRL + '/silent-renew.html',
            // silent_renew_url: this.uRL + '/forbidden',
            // post_login_route: this.uRL + '/admin',
      
            forbidden_route: '/forbidden',
            unauthorized_route: '/unauthorized',
            log_console_warning_active: true,
            log_console_debug_active: true,
            max_id_token_iat_offset_allowed_in_seconds: 10,
            storage: localStorage
          };
      
          const authWellKnownEndpoints: AuthWellKnownEndpoints = {
            issuer: this.stsURL,// + '/.well-known/openid-configuration/jwks',
            authorization_endpoint: this.stsURL + '/connect/authorize',
            token_endpoint: this.stsURL + '/connect/token',
            userinfo_endpoint: this.stsURL + '/connect/userinfo',
            end_session_endpoint: this.stsURL + '/connect/endsession',
            check_session_iframe: this.stsURL + '/connect/checksession',
            revocation_endpoint: this.stsURL + '/connect/revocation',
            introspection_endpoint: this.stsURL + '/connect/introspect',
            jwks_uri: this.stsURL + '/.well-known/openid-configuration/jwks',
          };
      
          this.oidcSecurityService.setupModule(config, authWellKnownEndpoints);

        if (this.oidcSecurityService.moduleSetup) {
            this.doCallbackLogicIfRequired();
        } else {
            this.oidcSecurityService.onModuleSetup.subscribe(() => {
                this.doCallbackLogicIfRequired();
            });
        }
        this.isAuthorizedSubscription = this.oidcSecurityService.getIsAuthorized().subscribe((isAuthorized => {
            this.isAuthorized = isAuthorized;
        }));

        this.oidcSecurityService.onAuthorizationResult.subscribe(
            (authorizationResult: AuthorizationResult) => {
                alert(JSON.stringify(authorizationResult));
                this.onAuthorizationResultComplete(authorizationResult);
            });
    }

    private onAuthorizationResultComplete(authorizationResult: AuthorizationResult) {

        console.log('Auth result received AuthorizationState:'
            + authorizationResult.authorizationState
            + ' validationResult:' + authorizationResult.validationResult);

        if (authorizationResult.authorizationState === AuthorizationState.unauthorized) {
            if (window.parent) {
                // sent from the child iframe, for example the silent renew
                this.router.navigate(['/unauthorized']);
            } else {
                window.location.href = '/unauthorized';
            }
        }
    }

    private doCallbackLogicIfRequired() {
        this.oidcSecurityService.authorizedCallbackWithCode(window.location.toString());
    }

    getIsAuthorized(): Observable<boolean> {
        return this.oidcSecurityService.getIsAuthorized();
    }

    login() {
        console.log('start login');
        this.oidcSecurityService.authorize();
    }

    logout() {
        console.log('start logoff');
        this.oidcSecurityService.logoff();
    }

    get(url: string): Observable<any> {
        return this.http.get(url, { headers: this.getHeaders() })
        .pipe(catchError((error) => {
            this.oidcSecurityService.handleError(error);
            return throwError(error);
        }));
    }

    put(url: string, data: any): Observable<any> {
        const body = JSON.stringify(data);
        return this.http.put(url, body, { headers: this.getHeaders() })
        .pipe(catchError((error) => {
            this.oidcSecurityService.handleError(error);
            return throwError(error);
        }));
    }

    delete(url: string): Observable<any> {
        return this.http.delete(url, { headers: this.getHeaders() })
        .pipe(catchError((error) => {
            this.oidcSecurityService.handleError(error);
            return throwError(error);
        }));
    }

    post(url: string, data: any): Observable<any> {
        const body = JSON.stringify(data);
        return this.http.post(url, body, { headers: this.getHeaders() })
        .pipe(catchError((error) => {
            this.oidcSecurityService.handleError(error);
            return throwError(error);
        }));
    }

    private getHeaders() {
        let headers = new HttpHeaders();
        headers = headers.set('Content-Type', 'application/json');
        return this.appendAuthHeader(headers);
    }

    public getToken() {
        const token = this.oidcSecurityService.getToken();
        return token;
    }

    public getUserData(){
        return this.oidcSecurityService.getUserData<{ email: string }>();
    }

    getUserinfo(): Observable<any>{
        return this.oidcSecurityService.getUserinfo();
    }

    private appendAuthHeader(headers: HttpHeaders) {
        
        const token = this.oidcSecurityService.getToken();

        if (token === '') { return headers; }

        const tokenValue = 'Bearer ' + token;
        return headers.set('Authorization', tokenValue);
    }
}
