import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgentTimeLineComponent } from './agent-time-line.component';

describe('AgentTimeLineComponent', () => {
  let component: AgentTimeLineComponent;
  let fixture: ComponentFixture<AgentTimeLineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgentTimeLineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgentTimeLineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
