import { Component, OnInit } from '@angular/core';
import { OidcSecurityService, AuthorizationResult } from 'angular-auth-oidc-client';
import { Router } from '@angular/router';
import { AuthService } from '../appAuth-service';

@Component({
  selector: 'app-forbidden',
  templateUrl: './forbidden.component.html',
  styleUrls: ['./forbidden.component.css']
})
export class ForbiddenComponent implements OnInit {

  constructor(private authService: AuthService) { }

  ngOnInit() {
    
  }

  public getData() {
    this.authService.getIsAuthorized().subscribe((a: boolean) => { alert(a) });
  }

}
