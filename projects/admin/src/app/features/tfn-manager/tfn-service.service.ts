import { Injectable } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TFN } from './tfn';

@Injectable({
  providedIn: 'root'
})
export class TfnServiceService {

  private baseURL: string = 'https://localhost:44311/resource/tfn';

  constructor(private httpClient: HttpClient) {

  }

  public getTFN(){
    return this.httpClient.get<TFN[]>(this.baseURL);
  }
}
