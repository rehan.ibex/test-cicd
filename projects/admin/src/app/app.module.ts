// import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ModuleWithProviders } from '@angular/compiler/src/core';
import { DashboardComponent } from './features/dashboard/dashboard.component';
import { SkillManagerComponent } from './features/skill-manager/skill-manager.component';
import { TfnManagerComponent } from './features/tfn-manager/tfn-manager.component';
import { DispositionManagerComponent } from './features/disposition-manager/disposition-manager.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    SkillManagerComponent,
    TfnManagerComponent,
    DispositionManagerComponent,
  ],
  imports: [
    // BrowserModule,
    AppRoutingModule,
    CommonModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

// @NgModule({})
// export class AdminSharedModule{
//   static forRoot(): ModuleWithProviders {
//     return {
//       ngModule: AppModule,
//       providers: []
//     }
//   }
// }