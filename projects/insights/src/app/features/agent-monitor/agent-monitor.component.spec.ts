import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgentMonitorComponent } from './agent-monitor.component';

describe('AgentMonitorComponent', () => {
  let component: AgentMonitorComponent;
  let fixture: ComponentFixture<AgentMonitorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgentMonitorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgentMonitorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
