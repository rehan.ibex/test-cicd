import { NgModule, ModuleWithProviders } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './features/dashboard/dashboard.component';
import { SkillMonitorComponent } from './features/skill-monitor/skill-monitor.component';
import { CallMonitorComponent } from './features/call-monitor/call-monitor.component';
import { AgentMonitorComponent } from './features/agent-monitor/agent-monitor.component';
import { UserManagerComponent } from './features/user-manager/user-manager.component';
import { AgentTimeLineComponent } from './features/agent-time-line/agent-time-line.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    SkillMonitorComponent,
    CallMonitorComponent,
    AgentMonitorComponent,
    UserManagerComponent,
    AgentTimeLineComponent
  ],
  imports: [
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }


// @NgModule({})
// export class InsightsSharedModule{
//   static forRoot(): ModuleWithProviders {
//     return {
//       ngModule: AppModule,
//       providers: []
//     }
//   }
// }