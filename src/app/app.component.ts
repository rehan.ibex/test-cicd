import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { OidcSecurityService } from 'angular-auth-oidc-client';
import { Subscriber } from 'rxjs';
import { AuthService } from './appAuth-service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  
  isAuthenticated:boolean;
  title = 'dgsconsole';

  ngOnInit(): void {
    this.authService.initAuth();
    this.authService.getIsAuthorized().subscribe((isAuth) => 
    {
      this.isAuthenticated = isAuth;
      if (isAuth) {
        this.router.navigate(['/dashboard']);
      }

    });
  }

  constructor(private authService: AuthService, private router: Router) {
    
  }
}
