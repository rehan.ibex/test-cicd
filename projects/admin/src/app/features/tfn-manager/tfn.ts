export interface TFN {
    id: number;
    name: string;
    skillCode: string;
    tfn: string;
    cableCompany: string;
    greeting: string;
    subTitle: string;
    baseLine?: any;
    createdByUserID?: any;
    counterID: number;
    netUID: string;
    created: Date;
    updated: Date;
}

