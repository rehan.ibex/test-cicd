import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './features/dashboard/dashboard.component';
import { SkillMonitorComponent } from './features/skill-monitor/skill-monitor.component';
import { CallMonitorComponent } from './features/call-monitor/call-monitor.component';
import { AgentMonitorComponent } from './features/agent-monitor/agent-monitor.component';
import { AgentTimeLineComponent } from './features/agent-time-line/agent-time-line.component';
import { UserManagerComponent } from './features/user-manager/user-manager.component';
import { AppComponent } from './app.component';

const routes: Routes = [
  {
    path: '', component: AppComponent,
    children: [
      {
        path: '',
        component: DashboardComponent
      },
      {
        path: 'dashboard',
        component: DashboardComponent
      },
      {
        path: 'skill',
        component: SkillMonitorComponent
      },
      {
        path: 'call',
        component: CallMonitorComponent
      },
      {
        path: 'agent',
        component: AgentMonitorComponent
      },
      {
        path: 'user',
        component: UserManagerComponent
      },
      {
        path: 'agenttimeline',
        component: AgentTimeLineComponent
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
