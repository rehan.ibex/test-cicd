import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { AppComponent } from './app.component';
import { BrowserModule } from '@angular/platform-browser';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ForbiddenComponent } from './forbidden/forbidden.component';
import { UnauthComponent } from './unauth/unauth.component';
import { AuthModule } from 'angular-auth-oidc-client';
import { HomeComponent } from './home/home.component';
const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'home', component: HomeComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'admin', loadChildren:()=> import('../../projects/admin/src/app/app.module').then(c=>c.AppModule ) },
  { path: 'agent', loadChildren:()=> import('../../projects/agent/src/app/app.module').then(c=>c.AppModule ) },
  { path: 'insights', loadChildren:()=> import('../../projects/insights/src/app/app.module').then(c=>c.AppModule ) },
  { path: 'forbidden', component: ForbiddenComponent },
  { path: 'unauthorized', component: UnauthComponent }
  //  { path: '**', component: DashboardComponent },
];

@NgModule({
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
    AuthModule.forRoot()
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
