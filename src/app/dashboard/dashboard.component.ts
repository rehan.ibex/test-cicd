import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OidcSecurityService, AuthorizationResult } from 'angular-auth-oidc-client';
import { debug } from 'util';
import { AuthService } from '../appAuth-service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent {

  constructor(private authService: AuthService, private httpClient: HttpClient) {
  }

  public getData() {
    
    let apiURL = 'https://localhost:44342/api/values';
    let token = this.authService.getToken();
    alert(token);

    this.authService.getUserData().subscribe(a => {
      alert(JSON.stringify(a));
    });

    this.authService.get(apiURL).subscribe((a) => {
      alert(a);
    }, (err) => {
      alert(err);
    });
  }
}
