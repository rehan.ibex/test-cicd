import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TfnManagerComponent } from './tfn-manager.component';

describe('TfnManagerComponent', () => {
  let component: TfnManagerComponent;
  let fixture: ComponentFixture<TfnManagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TfnManagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TfnManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
