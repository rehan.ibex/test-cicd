import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './features/dashboard/dashboard.component';
import { SkillManagerComponent } from './features/skill-manager/skill-manager.component';
import { AppComponent } from './app.component';
import { TfnManagerComponent } from './features/tfn-manager/tfn-manager.component';
import { DispositionManagerComponent } from './features/disposition-manager/disposition-manager.component';


const routes: Routes = [
  {
    path: '', component: AppComponent, children:
      [
        {
          path: '',
          component: DashboardComponent
        },
        {
          path: 'dashboard',
          component: DashboardComponent
        },
        {
          path: 'skill',
          component: SkillManagerComponent
        },
        {
          path: 'tfn',
          component: TfnManagerComponent
        },
        {
          path: 'disposition',
          component: DispositionManagerComponent
        }
      ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
