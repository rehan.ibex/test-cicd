import { Component, OnInit } from '@angular/core';
import { TfnServiceService } from './tfn-service.service';
import { TFN } from './tfn';

@Component({
  selector: 'app-tfn-manager',
  templateUrl: './tfn-manager.component.html',
  styleUrls: ['./tfn-manager.component.css']
})
export class TfnManagerComponent implements OnInit {

 listTFNs: TFN[] = [];
// listTFNs: string[] = [];

  constructor(private tfnService: TfnServiceService) { }

  ngOnInit() {
    this.tfnService.getTFN().subscribe(a => {
      this.listTFNs = a;
    }
  );
  }
}
