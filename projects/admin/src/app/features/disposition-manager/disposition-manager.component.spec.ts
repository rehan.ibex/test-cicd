import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DispositionManagerComponent } from './disposition-manager.component';

describe('DispositionManagerComponent', () => {
  let component: DispositionManagerComponent;
  let fixture: ComponentFixture<DispositionManagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DispositionManagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DispositionManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
