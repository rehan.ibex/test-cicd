import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SkillMonitorComponent } from './skill-monitor.component';

describe('SkillMonitorComponent', () => {
  let component: SkillMonitorComponent;
  let fixture: ComponentFixture<SkillMonitorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SkillMonitorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SkillMonitorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
