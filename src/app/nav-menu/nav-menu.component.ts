import { Component, OnInit } from '@angular/core';
import { AuthService } from '../appAuth-service';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent implements OnInit {
  isLoggedIn: boolean = false;
  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.authService.getIsAuthorized().subscribe((isAuth) => {
      this.isLoggedIn = isAuth;
    });
  }

  public login() {
    this.authService.login();
  }

  public logout() {
    this.authService.logout();
  }

}
