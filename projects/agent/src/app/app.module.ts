import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ModuleWithProviders } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';

import { DashboardComponent } from './features/dashboard/dashboard.component';
import { AnalyzerComponent } from './features/analyzer/analyzer.component';


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    AnalyzerComponent,
  ],
  imports: [
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

// @NgModule({})
// export class AgentSharedModule{
//   static forRoot(): ModuleWithProviders {
//     return {
//       ngModule: AppModule,
//       providers: []
//     }
//   }
// }
