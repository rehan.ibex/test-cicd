import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';

import { HttpClientModule } from '@angular/common/http';
import { AuthModule, ConfigResult, OidcConfigService, OidcSecurityService, OpenIdConfiguration, AuthWellKnownEndpoints } from 'angular-auth-oidc-client';
import { ForbiddenComponent } from './forbidden/forbidden.component';
import { UnauthComponent } from './unauth/unauth.component';
import { AuthService } from './appAuth-service';
import { HomeComponent } from './home/home.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';

// import { AdminSharedModule } from 'projects/admin/src/app/app.module';
// import { AgentSharedModule } from 'projects/agent/src/app/app.module';
// import { InsightsSharedModule } from 'projects/insights/src/app/app.module';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    ForbiddenComponent,
    UnauthComponent,
    HomeComponent,
    NavMenuComponent,
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    // AdminSharedModule.forRoot(),
    // AgentSharedModule.forRoot(),
    // InsightsSharedModule.forRoot()
  ],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule {
  // private stsURL = "https://localhost:44328";
  private stsURL = "https://localhost:44352";
  private uRL = "https://localhost:4200";

  // constructor(public oidcSecurityService: OidcSecurityService) {
    // const config: OpenIdConfiguration = {
    //   stsServer: this.stsURL,
    //   redirect_url: this.uRL + '/forbidden',
    //   client_id: 'codeflowpkceclient',
    //   response_type: 'code', // 'id_token token' Implicit Flow
    //   scope: 'openid profile offline_access email',
    //   post_logout_redirect_uri: this.uRL,
    //   //start_checksession: true,
    //   // silent_renew: true,
    //   // silent_renew_url: this.uRL + '/silent-renew.html',
    //   // silent_renew_url: this.uRL + '/forbidden',
    //   // post_login_route: this.uRL + '/admin',

    //   forbidden_route: '/forbidden',
    //   unauthorized_route: '/unauthorized',
    //   log_console_warning_active: true,
    //   log_console_debug_active: true,
    //   max_id_token_iat_offset_allowed_in_seconds: 10,
    //   storage: localStorage
    // };

    // const authWellKnownEndpoints: AuthWellKnownEndpoints = {
    //   issuer: this.stsURL + '/.well-known/openid-configuration/jwks',
    //   authorization_endpoint: this.stsURL + '/connect/authorize',
    //   token_endpoint: this.stsURL + '/connect/token',
    //   userinfo_endpoint: this.stsURL + '/connect/userinfo',
    //   end_session_endpoint: this.stsURL + '/connect/endsession',
    //   check_session_iframe: this.stsURL + '/connect/checksession',
    //   revocation_endpoint: this.stsURL + '/connect/revocation',
    //   introspection_endpoint: this.stsURL + '/connect/introspect',
    // };

    // this.oidcSecurityService.setupModule(config, authWellKnownEndpoints);
  // }
}
